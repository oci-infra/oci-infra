terraform {
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = "5.21.0"
    }
    hcp = {
      source  = "hashicorp/hcp"
      version = "0.77.0"
    }
  }
  backend "http" {
  }
}

provider "oci" {
  tenancy_ocid = data.hcp_vault_secrets_secret.tenancy_ocid.secret_value
  user_ocid    = data.hcp_vault_secrets_secret.user_ocid.secret_value
  private_key  = data.hcp_vault_secrets_secret.private_key.secret_value
  fingerprint  = var.fingerprint
  region       = data.hcp_vault_secrets_secret.region.secret_value
}

provider "hcp" {
}
