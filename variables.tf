variable "fingerprint" {
  description = "Fingerprint for the key pair being used"
  type        = string
  default     = "08:63:98:7d:ed:6a:4b:4a:a4:aa:9b:56:a4:c3:1d:0a"
}

variable "humming_vcn_ipv4_cidrs" {
  description = "The list of one or more IPv4 CIDR blocks for the VCN "
  type        = list(string)
  default     = ["10.11.0.0/23"]
}

variable "humming_vcn_name" {
  description = "A user-friendly name"
  type        = string
  default     = "humming"
}

variable "humming_vcn_dns" {
  description = "A DNS label for the VCN"
  type        = string
  default     = "humming"
}

variable "humming_vcn_nat_name" {
  description = "A user-friendly name for nat gateway"
  type        = string
  default     = "humming-nat"
}

variable "humming_vcn_subnet1_ipv4_cidr" {
  description = "The CIDR IP address range of the subnet"
  type        = string
  default     = "10.11.0.0/24"
}

variable "humming_vcn_subnet1_name" {
  description = " A user-friendly name"
  type        = string
  default     = "subnet1"
}

variable "humming_vcn_subnet1_dns" {
  description = "A DNS label for the subnet"
  type        = string
  default     = "subnet1"
}

variable "humming_vcn_subnet1_prohibit_ingress" {
  description = "Whether to disallow ingress internet traffic to VNICs within this subnet"
  type        = bool
  default     = false
}

variable "humming_vcn_subnet1_ipv6_cidr" {
  description = "Use this to enable IPv6 addressing for this subnet"
  type        = string
  default     = "2603:c026:302:de00::/64"
}
