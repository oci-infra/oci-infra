resource "oci_identity_compartment" "tf_compartment" {
  compartment_id = data.hcp_vault_secrets_secret.tenancy_ocid.secret_value
  description    = "Compartment for Terraform resources."
  name           = "tf"
}
