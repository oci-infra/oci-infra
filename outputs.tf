output "compartment-OCID" {
  value = oci_identity_compartment.tf_compartment.id
}

output "vcn-OCID" {
  value = oci_core_vcn.humming.id
}

output "vcn-subnet-OCID" {
  value = oci_core_subnet.subnet1.id
}
