data "hcp_vault_secrets_secret" "tenancy_ocid" {
  app_name    = "gitlabociinfra"
  secret_name = "oci_tenancy_ocid"
}

data "hcp_vault_secrets_secret" "user_ocid" {
  app_name    = "gitlabociinfra"
  secret_name = "oci_user_ocid"
}

data "hcp_vault_secrets_secret" "region" {
  app_name    = "gitlabociinfra"
  secret_name = "oci_region"
}

data "hcp_vault_secrets_secret" "private_key" {
  app_name    = "gitlabociinfra"
  secret_name = "oci_private_key"
}
