resource "oci_core_vcn" "humming" {
  compartment_id                   = oci_identity_compartment.tf_compartment.id
  cidr_blocks                      = var.humming_vcn_ipv4_cidrs
  display_name                     = var.humming_vcn_name
  dns_label                        = var.humming_vcn_dns
  is_ipv6enabled                   = true
  is_oracle_gua_allocation_enabled = true
}

resource "oci_core_subnet" "subnet1" {
  compartment_id            = oci_identity_compartment.tf_compartment.id
  cidr_block                = var.humming_vcn_subnet1_ipv4_cidr
  vcn_id                    = oci_core_vcn.humming.id
  display_name              = var.humming_vcn_subnet1_name
  dns_label                 = var.humming_vcn_subnet1_dns
  prohibit_internet_ingress = var.humming_vcn_subnet1_prohibit_ingress
  ipv6cidr_block            = var.humming_vcn_subnet1_ipv6_cidr
}

resource "oci_core_internet_gateway" "humming_gw" {
  compartment_id = oci_identity_compartment.tf_compartment.id
  vcn_id         = oci_core_vcn.humming.id
  enabled        = true
  display_name   = "gw0"
}

resource "oci_core_default_route_table" "humming_default_rt" {
  manage_default_resource_id = oci_core_vcn.humming.default_route_table_id
  route_rules {
    network_entity_id = oci_core_internet_gateway.humming_gw.id
    destination_type  = "CIDR_BLOCK"
    destination       = "::/0"
  }
  route_rules {
    network_entity_id = oci_core_internet_gateway.humming_gw.id
    destination_type  = "CIDR_BLOCK"
    destination       = "0.0.0.0/0"
  }
}
